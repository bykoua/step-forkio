const burgerBtn = document.querySelector(".burger-menu");
const burgerMenu = document.querySelector(".navbar__menu");

burgerBtn.addEventListener("click", () => {
    burgerBtn.classList.toggle("burger-menu--active");
    burgerMenu.classList.toggle("navbar__menu--active");
});
