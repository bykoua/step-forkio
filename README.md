# FORKIO

[Посилання на GitLab Pages](https://bykoua.gitlab.io/step-forkio/)

### Над проєктом працювали студенти групи fe-3_online: Бобир Ганна и Биков Микита.

### В данному проєкті були використані наступні технології: 

Для збірки проєкту - gulp.
Для поділення одного html файла на декілька - gulp-file-include.
Для написання стилів - препроцессор SASS.
Для сумісності стилів з браузерами та їх версіями - gulp-autprefixer.
Для видалення невикористаного css-кода - gulp-clean-css.
Для мініфікації js-кода - gulp-js-mßinify.
Для мініфікації зображень різних форматів - gulp-imagemin.
Для поєднання декількох файлів в один (конкатенації) - gulp-concat.
Для очищення фінальної директорії dist - gulp-clean.

#### Основний git-репозиторій - gitlab.

#### Бобир Ганна:

Налаштування gulpfile.js.

Верстка секції Revolutionary Editor.
Верстка секції Here is what you get.
Верстка секції Fork Subscription Pricing.

#### Биков Микита:

Налаштування git-репозиторія.
Налаштування gulpfile.js.

Верстка секції Navbar
Верстка секції Header
Верстка секції People Are Talking About Fork.
