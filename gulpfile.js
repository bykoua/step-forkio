const {series, parallel, watch, src, dest} = require("gulp");
const browserSync = require("browser-sync").create();
const clean = require('gulp-clean');
const autoprefixer = require('gulp-autoprefixer');
const minifyInline = require('gulp-minify-inline');
const concat = require('gulp-concat');
const rename = require("gulp-rename");
const htmlInclude = require('gulp-file-include');
const sass = require('gulp-sass')(require('sass'));
const cleanCSS = require('gulp-clean-css');
const minifyJs = require('gulp-js-minify');
const imagemin = require('gulp-imagemin');


const cleanDist = () => {
    return src(['dist/', './index.html'], {read: false, allowEmpty: true})
        .pipe(clean());
};

const serv = () => {
    browserSync.init({
        server: {
            baseDir: "./",
        },
    });
};

const bsReload = (cb) => {
    browserSync.reload();
    cb();
};

const html = () => {
    return src('./src/index.html')
        .pipe(htmlInclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(dest('./'))
        .pipe(browserSync.stream());
};

const styles = () => {
    return src("./src/scss/style.scss")
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(dest("./dist/css/"))
        .pipe(browserSync.stream());
};

const js = () => {
    return src('src/components/**/*.js')
        .pipe(concat('script.js'))
        .pipe(minifyInline())
        .pipe(minifyJs())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(dest('dist/js'))
        .pipe(browserSync.stream());
};
const img = () => {
    return src("./src/img/**/*.+(png|jpg|gif|svg|json|ico|xml)")
        .pipe(imagemin())
        .pipe(dest('./dist/img'))
        .pipe(browserSync.stream());
};

const watcher = (cb) => {
    watch("./src/**/*.html", html);
    watch("./src/**/*.scss", styles);
    watch("./src/**/*.js", js);
    watch("./src/img/*", img);
    cb();
};

exports.dev = parallel(serv, watcher, series(html, styles, js, img));
exports.build = series(cleanDist, html, styles, js, img);